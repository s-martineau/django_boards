[![pipeline status](https://gitlab.com/s-martineau/django_boards/badges/master/pipeline.svg)](https://gitlab.com/s-martineau/django_boards/-/commits/master)
[![coverage report](https://gitlab.com/s-martineau/django_boards/badges/master/coverage.svg)](https://gitlab.com/s-martineau/django_boards/-/commits/master)
# Django boards

**Note**: This website is available at https://boards.marsimon.com

Boards web app built using django

## Installation
Clone this repository using:
```bash
git clone https://gitlab.com/s-martineau/django_boards.git
```
Install dependencies using:
```bash
cd django_boards
python -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

To run the application, the environment variable "SECRET_KEY" must be defined:
```bash
export SECRET_KEY="test secret key"
```

Finally, migrate the database and create a super user to log in and follow the steps (**make sure the virtual environment is activated**):
```bash
python manage.py migrate
python manage.py createsuperuser
```

To run the server locally, use:
```bash
python manage.py runserver
```
The website is available locally at: http://localhost:8000

## Author

**Simon Martineau** -  [Github](https://github.com/simon-martineau)

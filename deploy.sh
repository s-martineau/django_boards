#!/bin/bash

GREEN="\033[0;32m"
NC="\033[0m"

USER="simon"
REMOTEHOST="165.22.226.28"
TARGET="/home/${USER}/django_boards"
WHAT_TO_COPY=(app instance nginx postgres docker-compose.yml init-letsencrypt.sh)

start=`date +%s`

echo -e "${GREEN}Deploying to server...${NC}"
echo -e "${GREEN}Ensure $TARGET exists in server...${NC}"
ssh $USER@$REMOTEHOST "mkdir -p ${TARGET}"

echo -e "${GREEN}Copying files to remote server...${NC}"
for i in "${WHAT_TO_COPY[@]}"; do
	rsync -r ./${i} $USER@$REMOTEHOST:$TARGET
done

echo -e "${GREEN}Performing ssh into server.${NC}"
COMMANDS=(
"docker-compose -f ${TARGET}/docker-compose.yml down --remove-orphans"
"docker-compose -f ${TARGET}/docker-compose.yml up -d --build"
)
for i in "${COMMANDS[@]}"; do
	echo -e "${GREEN}Running: ${i} ${NC}"
	ssh ${USER}@${REMOTEHOST} "${i}"
done

end=`date +%s`
runtime=$((end-start))
echo -e "${GREEN}Exec time: ${runtime}${NC}"
echo -e "${GREEN}Deployed to production${NC}"

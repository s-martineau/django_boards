$('.js-validate-username').change(function () {
    const username = $(this).val();
    const element = $(this);
    $.ajax({
        url: $(this).closest('form').attr('data-validate-username-url'),
        data: {
            'username': username
        },
        dataType: 'json',
        success: function (data) {
            element.closest('form').find('.form-errors').empty();
            if (data.is_taken) {
                element.closest('form').find('.form-errors').append(`<span>${data.message}</span>`);
                element.closest('form').find('button[type="submit"]').prop('disabled', true);

            } else {
                element.closest('form').find('button[type="submit"]').prop('disabled', false);
            }
        }
    });
});

$('.js-validate-topic-name').change(function () {
    const name = $(this).val();
    const element = $(this);
    $.ajax({
        url: $(this).closest('form').attr('data-validate-topic-name-url'),
        data: {
            'name': name
        },
        dataType: 'json',
        success: function (data) {
            element.closest('form').find('.form-errors').empty();
            if (data.is_taken) {
                element.closest('form').find('.form-errors').append(`<span>${data.message}</span>`);
                element.closest('form').find('button[type="submit"]').prop('disabled', true);

            } else {
                element.closest('form').find('button[type="submit"]').prop('disabled', false);
            }
        }
    });
});

$('#dismiss-welcome-banner').click(function () {
    const element = $(this);
    $.ajax({
        type: 'POST',
        url: element.attr('data-dismiss-welcome-banner-url'),
        data: {
            'hide_welcome_banner': true,
            'csrfmiddlewaretoken': window.CSRF_TOKEN
        },
        dataType: 'json'
    });
});

$('#collapseReply').on({
    'show.bs.collapse': function () {
        $('#openAddReply').css({'display': 'none'});
        window.scrollTo()
    },
    'shown.bs.collapse': function () {
        $('#openAddReply').css({'opacity': '0'});
    },
    'hidden.bs.collapse': function () {
        $('#openAddReply').css({'opacity': '100%'});
    },
    'hide.bs.collapse': function () {
        $('#openAddReply').css({'display': 'inline-block'});
    }
});


$('#confirmDeleteModal').on('show.bs.modal', function (event) {
    const button = $(event.relatedTarget);
    const ajaxUrl = button.attr('data-delete-post-url');
    const targetId = button.attr('delete-target-post');
    const targetDiv = button.attr('delete-target-div');

    $(this).find('#confirmDeleteButton').off('click').on('click', function () {
        $.ajax({
            type: 'POST',
            url: ajaxUrl,
            data: {
                'id': targetId,
                'csrfmiddlewaretoken': window.CSRF_TOKEN
            },
            dataType: 'json',
            success: function (data, statusText, xhr) {
                if (xhr.status === 200) {
                    console.log($(targetDiv.toString()));
                    $(targetDiv.toString()).fadeOut(
                        400, 'linear', () => $(this).remove()
                    );
                    console.log(data);
                    // TODO: Notify success
                } else {
                    // TODO: Notify error (display message)
                }
            }
        });
    });
})


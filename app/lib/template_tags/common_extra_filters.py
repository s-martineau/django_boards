from django import template


register = template.Library()


@register.filter(name='addcss')
def addcss(field, css):
    return field.as_widget(attrs={"class": css})


@register.filter(name='css_and_ph')
def placeholder(field, args: str):
    args = args.split(',')
    return field.as_widget(attrs={"class": args[0], "placeholder": args[1]})

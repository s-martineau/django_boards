from django.shortcuts import redirect, reverse
from django.contrib.auth.mixins import AccessMixin


class LoginExcludedMixin(AccessMixin):

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect(reverse(self.redirect_url))
        return super().dispatch(request, *args, **kwargs)

    @property
    def redirect_url(self):
        raise NotImplementedError('redirect_url property is not implemented')

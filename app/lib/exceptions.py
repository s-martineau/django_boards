

class IllegalCallError(Exception):
    """Raised when a method should not be called on a model based on its state"""

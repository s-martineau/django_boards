from django.test import TestCase
from django.contrib.auth.models import User
from django.shortcuts import reverse
from boards.models import Board, Topic, Post


class DeletePostTests(TestCase):

    def setUp(self):
        self.user = User.objects.create_user(
            username='testuser', email='test@test.com', password='very_secret',

        )
        self.user.refresh_from_db()
        self.test_board = Board.objects.create(name='test board', description='board test description')
        self.test_topic = Topic.objects.create(name='test topic', board=self.test_board, starter=self.user.profile)
        self.test_post = Post.objects.create(author=self.user.profile, topic=self.test_topic, message='test message')

    def test_post_delete_not_found(self):
        superuser = User.objects.create_superuser('testsuperuser', 'testemail@mail.com', 'password123')
        self.client.force_login(superuser)
        response = self.client.post(reverse('ajax:delete_post'), data={'id': 99})
        self.assertEquals(response.status_code, 404)
        self.assertEquals(Post.objects.all().count(), 1)

    def test_post_delete_unauthorized(self):
        response = self.client.post(reverse('ajax:delete_post'), data={'id': self.test_post.id})

        self.assertEquals(response.status_code, 401)
        self.assertEquals(Post.objects.all().count(), 1)

    def test_post_delete_authorized(self):
        superuser = User.objects.create_superuser('testsuperuser', 'testemail@mail.com', 'password123')
        self.client.force_login(superuser)
        response = self.client.post(reverse('ajax:delete_post'), data={'id': self.test_post.id})
        self.assertEquals(response.status_code, 200)
        self.assertEquals(Post.objects.all().count(), 0)


class DismissWelcomeBannerTests(TestCase):

    def test_toggle_hide(self):
        response = self.client.get(reverse('boards:index'))
        self.assertContains(response, 'id="collapseJumbotron"')
        ajax_resonse = self.client.post(reverse('ajax:dismiss_welcome_banner'), data={'hide_welcome_banner': True})
        self.assertEquals(ajax_resonse.status_code, 200)
        response2 = self.client.get(reverse('boards:index'))
        self.assertNotContains(response2, 'id="collapseJumbotron"')

    def test_bad_request(self):
        ajax_response = self.client.post(reverse('ajax:dismiss_welcome_banner'), data={'uselessdata': 'hi'})
        self.assertEquals(ajax_response.status_code, 400)


class ValidateUsernameTests(TestCase):

    def setUp(self):
        self.user = User.objects.create_user(
            username='testuser', email='test@test.com', password='very_secret',
        )

    def test_username_already_taken(self):
        response = self.client.get(reverse('ajax:validate_username'), data={'username': 'testuser'})
        self.assertEquals(response.status_code, 200)
        self.assertEquals(response.json()['is_taken'], True)

    def test_username_valid(self):
        response = self.client.get(reverse('ajax:validate_username'), data={'username': 'henlo'})
        self.assertEquals(response.status_code, 200)
        self.assertEquals(response.json()['is_taken'], False)

    def test_bad_request(self):
        response = self.client.get(reverse('ajax:validate_username'), data={'useless_data': 'henlo'})
        self.assertEquals(response.status_code, 401)
        self.assertIn('message', response.json())


class ValidateTopicNameTests(TestCase):

    def setUp(self):
        self.user = User.objects.create_user(
            username='test.user', email='test@test.com', password='very_secret',
        )
        self.test_board = Board.objects.create(name='test board', description='board test description')
        self.test_topic = Topic.objects.create(name='test topic', board=self.test_board, starter=self.user.profile)
        self.test_post = Post.objects.create(author=self.user.profile, topic=self.test_topic, message='test message')
        self.client.force_login(self.user)

    def test_topic_name_already_taken(self):
        response = self.client.get(reverse('ajax:validate_topic_name'), data={'name': 'test topic'})
        self.assertEquals(response.status_code, 200)
        self.assertEquals(response.json()['is_taken'], True)

    def test_topic_name_valid(self):
        response = self.client.get(reverse('ajax:validate_topic_name'), data={'name': 'asdfasdf'})
        self.assertEquals(response.status_code, 200)
        self.assertEquals(response.json()['is_taken'], False)

    def test_bad_request(self):
        response = self.client.get(reverse('ajax:validate_topic_name'), data={'uselessdata': 'henlo'})
        self.assertEquals(response.status_code, 401)
        self.assertIn('message', response.json())

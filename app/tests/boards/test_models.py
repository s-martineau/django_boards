from django.contrib.auth.models import User
from django.test import TestCase

from boards.models import Board, Topic, Post


class BoardTests(TestCase):

    def setUp(self):
        self.test_board = Board.objects.create(name=' test board', description='test board description')

    def test_save_applies_slug(self):
        self.assertEquals(self.test_board.slug, 'test-board')
        self.test_board.name = 'changed name'
        self.test_board.save()
        self.assertEquals(self.test_board.slug, 'changed-name')

    def test_get_url(self):
        self.assertEquals(self.test_board.get_url(), '/boards/test-board')


class TopicTests(TestCase):

    def setUp(self):
        self.user = User.objects.create_user(
            username='testuser', email='test@test.com', password='very_secret',
        )
        self.user2 = User.objects.create_user(
            username='testuser2', email='test2@test.com', password='very_secret2',
        )
        self.test_board = Board.objects.create(name=' test board', description='test board description')
        self.test_topic = Topic.objects.create(name='test topic', board=self.test_board, starter=self.user.profile)
        self.test_post = Post.objects.create(author=self.user.profile, message='hello world!', topic=self.test_topic)

    def test_updated_by(self):
        self.assertEquals(self.test_topic.updated_by.user, self.user)
        new_post = Post.objects.create(author=self.user2.profile, message='new test post', topic=self.test_topic)
        self.assertEquals(self.test_topic.updated_by.user, self.user2)
        self.assertTrue(new_post in self.test_topic.posts.all())

    def test_name_is_available_for_user(self):
        result = Topic.name_is_available_for_user(self.user, 'test topic')
        self.assertFalse(result)
        result2 = Topic.name_is_available_for_user(self.user, 'asdfasdf')
        self.assertTrue(result2)

    def test_get_url(self):
        self.assertEquals(self.test_topic.get_url(), f'/boards/{self.test_board.slug}/{self.test_topic.id}')


class PostTests(TestCase):

    def setUp(self):
        self.user = User.objects.create_user(
            username='testuser', email='test@test.com', password='very_secret',
        )
        self.user2 = User.objects.create_user(
            username='testuser2', email='test2@test.com', password='very_secret2',
        )
        self.test_board = Board.objects.create(name=' test board', description='test board description')
        self.test_topic = Topic.objects.create(name='test topic', board=self.test_board, starter=self.user.profile)
        self.test_post = Post.objects.create(author=self.user.profile, message='hello world!', topic=self.test_topic)

    def test_save(self):
        initial_time_board = self.test_board.last_updated
        initial_time_topic = self.test_topic.last_updated
        self.test_post.message = 'changed message'
        self.test_post.save()
        self.assertNotEqual(self.test_board.last_updated, initial_time_board)
        self.assertNotEqual(self.test_topic.last_updated, initial_time_topic)

    def test_get_url(self):
        self.assertEquals(self.test_post.get_url(), f'/boards/{self.test_board.slug}/{self.test_topic.id}')

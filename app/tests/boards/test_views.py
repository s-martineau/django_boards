from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse

from boards.models import Board, Topic, Post


# Known limitations:
# - The topic/post ratio displayed in the board view is not tested

class IndexViewTests(TestCase):

    def setUp(self):
        self.user = User.objects.create_user(
            username='testuser', email='test@test.com', password='very_secret',
        )
        self.test_board = Board.objects.create(name='testing', description='test description')

    def test_index_fresh_user(self):
        response = self.client.get(reverse('boards:index'))

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Welcome to Boards!')
        self.assertContains(response, 'Log in')
        self.assertContains(response, 'Sign up')
        self.assertContains(response, self.test_board.name)

    def test_index_user_dismiss_welcome_banner(self):
        session = self.client.session
        session['hide_welcome_banner'] = True
        session.save()

        response = self.client.get(reverse('boards:index'))

        self.assertEquals(response.status_code, 200)
        self.assertNotContains(response, 'Welcome to Boards!')
        self.assertContains(response, self.test_board.name)

    def test_index_authenticated_user(self):
        self.client.force_login(self.user)
        response = self.client.get(reverse('boards:index'))

        self.assertEquals(response.status_code, 200)
        self.assertNotContains(response, 'Welcome to Boards')
        self.assertContains(response, 'Account')
        self.assertContains(response, self.test_board.name)

    def test_account_dropdown_shows_for_authenticated_user(self):
        self.client.force_login(self.user)
        response = self.client.get(reverse('boards:index'))

        self.assertContains(response, 'Account')
        self.assertContains(response, 'Log out')

    def test_login_signup_shows_for_anon_visitor(self):
        response = self.client.get(reverse('boards:index'))

        self.assertContains(response, 'Log in', 2)
        self.assertContains(response, 'Sign up', 2)


class BoardViewTests(TestCase):

    def setUp(self):
        self.user = User.objects.create_user(
            username='testuser', email='test@test.com', password='very_secret',
        )
        self.user.refresh_from_db()
        self.test_board = Board.objects.create(name='testboard', description='board test description')
        self.test_topic = Topic.objects.create(name='test topic',
                                               board=self.test_board, starter=self.user.profile)

    def test_board_view_shows(self):
        url = reverse('boards:board', args=(self.test_board.slug,))
        response = self.client.get(url)
        self.assertEquals(response.status_code, 200)
        self.assertContains(response, 'testboard')
        self.assertContains(response, 'board test description')

    def test_link_to_board_is_valid(self):
        url = reverse('boards:board', args=(self.test_board.slug,))
        response = self.client.get(url)

        self.assertContains(
            response, 'href="{}"'.format(reverse('boards:topic', args=(self.test_board.slug, self.test_topic.id,))))


class TopicViewTests(TestCase):

    def setUp(self):
        self.user = User.objects.create_user(
            username='testuser', email='test@test.com', password='very_secret',

        )
        self.user.refresh_from_db()
        self.test_board = Board.objects.create(name='test board', description='board test description')
        self.test_topic = Topic.objects.create(name='test topic', board=self.test_board, starter=self.user.profile)
        self.test_post = Post.objects.create(author=self.user.profile, topic=self.test_topic, message='test message')

    def test_topic_view_works(self):
        request = self.client.get(reverse('boards:topic', args=(self.test_board.slug, self.test_topic.id,)))

        self.assertEquals(request.status_code, 200)
        self.assertContains(request, self.test_topic.name)
        self.assertContains(request, self.test_post.message)
        self.assertContains(request, self.test_board.name)


class CreateTopicViewTests(TestCase):

    def setUp(self):
        self.user = User.objects.create_user(
            username='testuser', email='test@test.com', password='very_secret',
        )
        self.user.refresh_from_db()
        self.test_board = Board.objects.create(name='testboard', description='board test description')
        self.test_topic = Topic.objects.create(name='test topic', board=self.test_board, starter=self.user.profile)
        self.view_url = reverse('boards:create_topic', args=(self.test_board.slug,))

    def test_get_view_not_authenticated(self):
        response = self.client.get(self.view_url)
        self.assertRedirects(response, '{}?next={}'.format(reverse('login'), self.view_url))

    def test_get_view_while_authenticated(self):
        self.client.force_login(self.user)
        response = self.client.get(self.view_url)
        self.assertEquals(response.status_code, 200)

    def test_post_valid_form(self):
        self.client.force_login(self.user)
        topic_count = Topic.objects.all().count()
        post_count = Post.objects.all().count()
        response = self.client.post(self.view_url, data={'name': 'asdfasdf', 'message': 'asdfasdfasdfasdf'})
        self.assertRedirects(response, reverse('boards:topic', args=(self.test_board.slug, self.test_topic.id + 1)))
        self.assertGreater(Topic.objects.all().count(), topic_count)
        self.assertGreater(Post.objects.all().count(), post_count)

    def test_post_invalid_name(self):
        self.client.force_login(self.user)
        topic_count = Topic.objects.all().count()
        post_count = Post.objects.all().count()
        response = self.client.post(self.view_url, data={'name': 'test topic', 'message': 'asdfasdfasdfasdf'})
        self.assertNotEquals(response.status_code, 302)
        self.assertEquals(Topic.objects.all().count(), topic_count)
        self.assertEquals(Post.objects.all().count(), post_count)

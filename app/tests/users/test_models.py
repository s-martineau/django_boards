from django.contrib.auth.models import User
from django.test import TestCase
from django.utils import timezone

from boards.models import Board, Topic, Post
from lib.exceptions import IllegalCallError


class ProfileTests(TestCase):

    def setUp(self):
        self.user = User.objects.create_user(
            username='testuser', email='test@test.com', password='very_secret',
        )
        self.user2 = User.objects.create_user(
            username='testuser2', email='test2@test.com', password='very_secret2',
        )
        self.profile1 = self.user.profile
        self.profile2 = self.user2.profile
        self.test_board = Board.objects.create(name=' test board', description='test board description')
        self.test_topic = Topic.objects.create(name='test topic', board=self.test_board, starter=self.user.profile)
        self.test_post = Post.objects.create(author=self.user.profile, message='hello world!', topic=self.test_topic)

    def test_name_can_be_changed(self):
        self.assertTrue(self.profile1.name_can_be_changed)
        self.profile2.name_update_date = timezone.now()
        self.assertFalse(self.profile2.name_can_be_changed)
        self.profile2.name_update_date = timezone.datetime(2010, 1, 1, tzinfo=timezone.utc)
        self.assertTrue(self.profile2.name_can_be_changed)

    def test_change_name(self):
        self.profile1.change_name('new_name')
        self.assertEquals(self.profile1.name, 'new_name')
        self.assertIsNotNone(self.profile1.name_update_date)
        self.assertRaises(IllegalCallError, self.profile1.change_name, 'asdf')

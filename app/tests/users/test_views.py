from django.contrib.auth.models import User
from django.contrib.auth import get_user
from django.test import TestCase
from django.urls import reverse


class SignupViewTests(TestCase):

    def setUp(self):
        self.user = User.objects.create_user(
            username='testuser', email='test@test.com', password='very_secret',
        )
        self.signup_url = reverse('signup')

    def test_get_view(self):
        response = self.client.get(self.signup_url)
        self.assertEquals(response.status_code, 200)

    def test_get_view_while_authenticated_redirects(self):
        self.client.force_login(self.user)
        response = self.client.get(self.signup_url)
        self.assertEquals(response.status_code, 302)

    def test_invalid_username(self):
        data = {
            'username': 'testuser', 'email': 'asdfasdf@test.com', 'password1': 'f9aw238jfa', 'password2': 'f9aw238jfa'
        }
        response = self.client.post(reverse('signup'), data=data)

        self.assertNotEquals(response.status_code, 302)
        self.assertEquals(User.objects.all().count(), 1)

    def test_account_creation(self):
        data = {
            'username': 'nodupl', 'email': 'asdfasdf@test.com', 'password1': 'f9aw238jfa', 'password2': 'f9aw238jfa'
        }
        response = self.client.post(reverse('signup'), data=data)

        self.assertEquals(response.status_code, 302)
        self.assertEquals(User.objects.all().count(), 2)


class LoginViewTests(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(
            username='testuser', email='test@test.com', password='very_secret',
        )
        self.login_url = reverse('login')

    def test_get_view(self):
        response = self.client.get(self.login_url)
        self.assertEquals(response.status_code, 200)

    def test_get_view_while_authenticated_redirects(self):
        self.client.force_login(self.user)
        response = self.client.get(self.login_url)
        self.assertEquals(response.status_code, 302)

    def test_successful_login(self):
        response = self.client.post(self.login_url, data={'username': 'testuser', 'password': 'very_secret'})
        self.assertEquals(response.status_code, 302)
        self.assertTrue(get_user(self.client).is_authenticated)

    def test_unsuccessful_login(self):
        response = self.client.post(self.login_url, data={'username': 'blabla', 'password': 'blabla'})
        self.assertEquals(response.status_code, 200)
        self.assertFalse(get_user(self.client).is_authenticated)

from django.contrib import admin
from . import models


@admin.register(models.Board)
class BoardAdmin(admin.ModelAdmin):
    fields = ('name', 'description')


admin.site.register(models.Topic)
admin.site.register(models.Post)

from django import forms
from .models import Topic, Board, Post
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field, Div, HTML
from tinymce.widgets import TinyMCE


class TopicCreationForm(forms.Form):
    name = forms.CharField()
    message = forms.CharField(
        widget=TinyMCE(attrs={'rows': 20})
    )

    def __init__(self, board, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.board = board
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            Div(
                Field('name', css_class='js-validate-topic-name'),
                Div(css_class='text-danger form-errors text-right'),
                style='max-width: 400px;'
            ),
            Field('message')
        )


class PostForm(forms.Form):
    message = forms.CharField(
        widget=TinyMCE
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.form_show_labels = False

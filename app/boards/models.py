from django.db import models
from django.contrib.auth.models import User
from users.models import Profile
from django.shortcuts import reverse
from django.utils import timezone
from django.utils.text import slugify


class Board(models.Model):
    name = models.CharField(unique=True, max_length=40)
    description = models.CharField(max_length=100)
    last_updated = models.DateTimeField(auto_now=True)
    slug = models.CharField(unique=False, max_length=40)
    date_created = models.DateField(auto_now_add=True)

    def __str__(self):
        return f'Board: "{self.name}"'

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super().save(*args, **kwargs)

    def get_url(self):
        return reverse('boards:board', args=(self.slug,))


class Topic(models.Model):
    name = models.CharField(max_length=100)
    last_updated = models.DateTimeField(auto_now=True)
    board = models.ForeignKey(Board, on_delete=models.CASCADE, related_name='topics')
    starter = models.ForeignKey(Profile, on_delete=models.SET_NULL, related_name='topics', null=True)
    created_on = models.DateTimeField(auto_now_add=True, editable=False)
    updated_by = models.ForeignKey(Profile, on_delete=models.SET_NULL, null=True, related_name='+')
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'Topic | Board: {self.board.name} | User: {self.starter.name}'

    def __repr__(self):
        return f'Board: {self.board.name} | Starter: {self.starter.name}'

    def get_url(self):
        return reverse('boards:topic', args=(self.board.slug, self.id))

    @staticmethod
    def name_is_available_for_user(user, name):
        return Topic.objects.filter(name=name, starter=user.profile).count() == 0


class Post(models.Model):
    message = models.TextField(max_length=4000)
    topic = models.ForeignKey(Topic, on_delete=models.CASCADE, related_name='posts')

    author = models.ForeignKey(Profile, on_delete=models.SET_NULL, null=True, related_name='posts')
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'Topic: {self.topic.name} | Author: {self.author.name}'

    def __repr__(self):
        return f'Post | Topic: {self.topic.name} | Author: {self.author.name}'

    def get_url(self):
        return reverse('boards:topic', args=(self.topic.board.slug, self.topic.id))

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        self.topic.board.last_updated = timezone.now()
        self.topic.last_updated = timezone.now()
        self.topic.updated_by = self.author
        self.topic.board.save()
        self.topic.save()

from django.urls import path
from django.shortcuts import redirect, reverse
from . import views

app_name = 'boards'
urlpatterns = [
    path('', views.index, name='index'),
    path('boards', lambda request: redirect(reverse('boards:index'), permanent=True)),
    path('boards/<slug:board_slug>', views.board, name='board'),
    path('boards/<slug:board_slug>/<int:topic_id>', views.topic, name='topic'),
    path('boards/<slug:board_slug>/create/topic', views.create_topic, name='create_topic'),
]

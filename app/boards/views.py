from django.shortcuts import render, get_object_or_404, redirect, reverse
from .models import Board, Topic, Post
from .forms import TopicCreationForm, PostForm
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django import http


def index(request):
    boards = Board.objects.all()
    for board in boards:
        topics = board.topics.all()
        board.topic_count = topics.count()
        board.post_count = 0
        for topic in topics:
            board.post_count += topic.posts.count()

    # Show welcome banner for anonymous users who have not dismissed the banner
    show_welcome_banner = request.session.get('show_welcome_banner', True) and not request.user.is_authenticated

    context = {'boards': boards, 'show_welcome_banner': show_welcome_banner}
    return render(request, 'boards/index.html', context=context)


def board(request, board_slug):
    board_ = get_object_or_404(Board, slug=board_slug)
    context = {'board': board_}
    return render(request, 'boards/board.html', context=context)


def topic(request, board_slug, topic_id):
    board_ = get_object_or_404(Board, slug=board_slug)
    topic_ = get_object_or_404(Topic, pk=topic_id)
    form = PostForm()
    if request.POST:
        message = request.POST['message']
        if message is not None:
            user = request.user
            Post.objects.create(author=user.profile, message=message, topic=topic_)

    context = {'board': board_, 'topic': topic_, 'form': form}
    return render(request, 'boards/topic.html', context)


@login_required
def create_topic(request, board_slug):
    board_ = get_object_or_404(Board, slug=board_slug)
    form = TopicCreationForm(board_)
    if request.method == 'POST':
        name = request.POST.get('name')
        message = request.POST.get('message')
        if Topic.name_is_available_for_user(request.user, name):
            new_topic = Topic.objects.create(name=name, board=board_, starter=request.user.profile)
            new_post = Post.objects.create(author=request.user.profile, topic=new_topic, message=message)
            return redirect(new_post.get_url())
        else:
            messages.warning(request, 'This topic name is not available')
    context = {'board': board_, 'form': form}
    return render(request, 'boards/create_topic.html', context)

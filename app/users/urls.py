from django.urls import path
from . import views

app_name = 'users'
urlpatterns = [
    path('profiles/<slug:profile_name>', views.profile_view, name='profile'),
]

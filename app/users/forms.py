from django import forms
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field, Div


class SignupForm(UserCreationForm):

    helper = FormHelper()
    helper.layout = Layout(
        Field('username', css_class='js-validate-username'),
        Div(css_class='text-danger form-errors text-right'),
        'email', 'password1', 'password2'
    )
    helper.form_tag = False

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['password1'].help_text = 'Minimum 8 characters. At least one non-numeric character.'
        self.fields['password2'].help_text = None

    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2')

        help_texts = {
            'username': 'Letters, digits and @/./+/-/_ only.',
            'password1': 'Minimum 8 characters. At least one non-numeric character.'
        }


class LoginForm(AuthenticationForm):
    pass


class ProfileForm(forms.Form):
    name = forms.CharField(max_length=40)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()

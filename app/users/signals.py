# signals.py

from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.db.utils import IntegrityError
from django.utils.text import slugify
from .models import Profile


@receiver(post_save, sender=User)
def create_user_profile(sender, instance: User, created, **kwargs):
    if created:
        # Add numbers to the default name if the name already exists
        try:
            Profile.objects.create(user=instance, name=slugify(instance.username))
        except IntegrityError:
            name_suffix = 2
            while True:
                try:
                    Profile.objects.create(user=instance, name=slugify(instance.username) + str(name_suffix))
                    break
                except IntegrityError:
                    name_suffix += 1


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()

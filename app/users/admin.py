from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from django.utils.html import mark_safe
from django.urls import reverse
from boards.models import Topic, Post
from . import models


class ProfileInline(admin.StackedInline):
    model = models.Profile
    can_delete = False
    verbose_name_plural = 'Profile'
    fk_name = 'user'


class PostInline(admin.TabularInline):
    model = Post
    fields = ('link_to_topic', 'message_preview',)
    readonly_fields = ('author', 'message_preview', 'link_to_topic')

    def link_to_topic(self, instance: Post):
        return mark_safe(
            '<a href="{}">{}</a>'.format(instance.get_url(), instance.topic.name)
        )

    def message_preview(self, instance: Post):
        if len(instance.message) < 50:
            return instance.message
        else:
            return '{:50}...'.format(instance.message)


@admin.register(models.Profile)
class ProfileAdmin(admin.ModelAdmin):
    fields = ('user_admin_link', 'name', 'image')
    readonly_fields = ('user_admin_link',)

    inlines = (PostInline,)

    def user_admin_link(self, instance):
        return mark_safe(
            '<a href={}>Link to user</a>'.format(reverse('admin:auth_user_change', args=(instance.user.id,))))

    user_admin_link.verbose_name = 'Related user'

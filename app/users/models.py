import secrets
import os

from django.db import models
from django.contrib.auth.models import User
from django.utils.deconstruct import deconstructible
from django.utils import timezone
from shortuuidfield import ShortUUIDField
from PIL import Image

from lib.exceptions import IllegalCallError


@deconstructible
class PathAndRename:
    def __init__(self, sub_path):
        self.path = sub_path

    def __call__(self, _, filename):
        extension = filename.split('.')[-1]
        name = secrets.token_hex(8)

        filename = '{}.{}'.format(name, extension)
        # return the whole path to the file
        return os.path.join(self.path, filename)


class Profile(models.Model):
    uuid = ShortUUIDField(auto=True, unique=True, null=False)
    user = models.OneToOneField(User, on_delete=models.CASCADE, editable=False)
    name = models.CharField(max_length=40, unique=True, blank=False)
    name_update_date = models.DateTimeField(null=True)
    image = models.ImageField(upload_to=PathAndRename('profile_images/'), default='profile_images/default.jpg')

    def __str__(self):
        return f'{self.name}' or '~Undefined~'

    def __repr__(self):
        return f'{self.name} ({self.user.username})'

    def change_name(self, new_name):
        if not self.name_can_be_changed:
            raise IllegalCallError("This profile's name cannot be changed yet")
        self.name = new_name
        self.name_update_date = timezone.now()

    @property
    def name_can_be_changed(self):
        if self.name_update_date is None:
            return True
        return timezone.now() - self.name_update_date >= timezone.timedelta(30)

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        image = Image.open(self.image.path)
        output_size = (250, 250)
        image.thumbnail(output_size)
        image.save(self.image.path)

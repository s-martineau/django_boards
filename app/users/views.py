from django.shortcuts import render, redirect, reverse, get_object_or_404
from django.contrib import messages
from django.contrib.auth import login, logout
from django.contrib.auth.views import LoginView

from users.forms import SignupForm, LoginForm, ProfileForm
from users.models import Profile
from lib.view_decorators import login_excluded
from lib.view_mixins import LoginExcludedMixin


@login_excluded('boards:index')
def signup_view(request):
    if request.method == 'POST':
        form = SignupForm(request.POST)
        if form.is_valid():
            user = form.save()
            messages.success(request, 'Your account has been sucessfully created!')
            login(request, user)
            return redirect(reverse('boards:index'))
    else:
        form = SignupForm()

    context = {'form': form}
    return render(request, 'users/signup.html', context=context)


def logout_view(request):
    if request.user.is_authenticated:
        logout(request)
        request.session['hide_welcome_banner'] = True
        messages.info(request, 'You have been logged out')
    return redirect('boards:index')


class CustomLoginView(LoginExcludedMixin, LoginView):
    @property
    def redirect_url(self):
        return 'boards:index'

    form_class = LoginForm


def profile_view(request, profile_name):
    profile = get_object_or_404(Profile, name=profile_name)
    try:
        own = request.user.profile.id == profile.id
    except AttributeError:
        own = False
    form = ProfileForm()

    context = {'profile_is_own': own, 'user': profile.user, 'form': form}

    return render(request, 'users/profile.html', context=context)

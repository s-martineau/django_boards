from django.contrib.auth.models import User
from django.http import JsonResponse
from boards.models import Post, Topic


def validate_username(request):
    try:
        username = request.GET['username']
    except KeyError:
        return JsonResponse({'message': 'parameter "username" should be specified'}, status=401)
    data = {
        'is_taken': User.objects.filter(username=username).exists()
    }
    if data['is_taken']:
        data['message'] = 'A user with this username already exists.'

    return JsonResponse(data)


def validate_topic_name(request):
    try:
        name = request.GET['name']
    except KeyError:
        return JsonResponse({'message': 'parameter "name" should be specified'}, status=401)
    data = {
        'is_taken': not Topic.name_is_available_for_user(request.user, name)
    }
    if data['is_taken']:
        data['message'] = 'This topic name is not available'

    return JsonResponse(data)


def dismiss_welcome_banner(request):
    try:
        value = request.POST['hide_welcome_banner']
    except KeyError:
        return JsonResponse({'message': '"hide_welcome_banner" is not specified'}, status=400)
    request.session['hide_welcome_banner'] = value
    return JsonResponse({})


def delete_post(request):
    if request.user.is_superuser and request.method == 'POST':
        try:
            pk = request.POST['id']
            post = Post.objects.get(pk=pk)
        except (Post.DoesNotExist, KeyError):
            return JsonResponse(
                {'message': 'The requested post could not be found by the server. It may have been deleted already.'},
                status=404
            )
        post.delete()
        return JsonResponse({})
    else:
        return JsonResponse({'message': 'You are not authorized to perform this operation'}, status=401)

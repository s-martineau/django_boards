from django.urls import path
from django.shortcuts import redirect, reverse
from . import views

app_name = 'ajax'
urlpatterns = [
    path('validate_username', views.validate_username, name='validate_username'),
    path('dismiss_welcome_banner', views.dismiss_welcome_banner, name='dismiss_welcome_banner'),
    path('validate_topic_name', views.validate_topic_name, name='validate_topic_name'),
    path('posts/delete', views.delete_post, name='delete_post'),
]
